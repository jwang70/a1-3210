#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>


int main(int argc, char *argv[])
{
	struct timeval start, end;
	int buffer_size = 0;
	if (argc != 4 && argc != 3) {
		printf("Incorrect number of arguments. Expected %d arguments, received %d", 4, argc);
		exit(0);
	}
	if (argc == 3){
		buffer_size = 4096;
	}
	else {
		buffer_size = atoi(argv[3]);
	}
	char * fileName = argv[1];
	char * ip_port = argv[2];
	char buffer[buffer_size + 1]; /* +1 so we can add null terminator */
	char *server_ip = strtok(ip_port, ":");
    char *server_port_str = strtok(NULL, ":");
	if (server_ip == NULL || server_port_str == NULL) {
        printf("Invalid server address format. Use 'server-IP-address:port-number'.\n");
        exit(0);
    }
	int portnum = atoi(server_port_str);
	FILE * file = fopen(fileName, "r");
	if (!file) {
		printf("Could not open file %s", fileName);
		exit(0);
	}
	int len, mysocket;
	struct sockaddr_in dest; // socket info about the machine connecting to us
 
	/* Create a socket.
	   The arguments indicate that this is an IPv4, TCP socket
	*/
	mysocket = socket(AF_INET, SOCK_STREAM, 0);
  
	memset(&dest, 0, sizeof(dest));                // zero the struct
	
	//Initialize the destination socket information
	dest.sin_family = AF_INET;					   // Use the IPv4 address family
	// if (argc == 1){
	// 	dest.sin_addr.s_addr = htonl(INADDR_LOOPBACK); // Set destination IP number - localhost, 127.0.0.1
	// }else if (argc == 2){
	// 	inet_aton(argv[1], &dest.sin_addr); // Set destination IP number from command line arg
	// }else{
	// 	printf("Invalid number of arguments\n");
	// 	printf("Usage: %s <ip>, where ip is an optional IP4 address in the dot-decimal notation\n", argv[0]);
	// 	printf("If the IP address is not proided, client attempts to connect to localhost\n");
	// 	exit(0);
	// }
	inet_aton(server_ip, &dest.sin_addr);
	dest.sin_port = htons(portnum);                // Set destination port number
 	
	// Connect to the server
	connect(mysocket, (struct sockaddr *)&dest, sizeof(struct sockaddr_in));
	gettimeofday(&start, NULL);
	int filename_length = strlen(fileName);
	send(mysocket, &filename_length, sizeof(int), 0);
	int length = send(mysocket, fileName, filename_length, 0);
	printf("\nSending filename %s with length %d", fileName, length);
	size_t bytes_read;
	while ((bytes_read = fread(buffer, 1, buffer_size, file)) > 0) {
        send(mysocket, buffer, bytes_read, 0);
		length += bytes_read;
    }
	fclose(file);
	gettimeofday(&end, NULL);
	printf("\n");
	double transfer_time = (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1e6;
	double transfer_rate = length/transfer_time;
	printf("\nTotal Transfer time: %.6f  Total Transfer rate: %.2f KB per second", transfer_time, transfer_rate/1000);
	// len = recv(mysocket, buffer, MAXRCVLEN, 0);
 
	/* We have to null terminate the received data ourselves */
	// buffer[len] = '\0';
 
	// printf("Received %s (%d bytes) from %s on port %d.\n", buffer, len, inet_ntoa(dest.sin_addr), ntohs(dest.sin_port));
 
	close(mysocket);
	return EXIT_SUCCESS;
}
