import subprocess

c_program_path = './TCPclient'
c_program_args = '127.0.0.1:56040'
c_program_files = ['file1.txt', 'file2.txt', 'file3.txt']

num_runs = 3

for i in range(num_runs):
    command = [c_program_path, c_program_files[i], f'{c_program_args}']
    # Run the C program as a subprocess
    print(command)
    result = subprocess.run([c_program_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    # Check if the C program ran successfully
    if result.returncode == 0:
        print(f"Run {i+1} successful:")
        print(result.stdout)
    else:
        print(f"Run {i+1} failed:")
        print(result.stderr)
